%global debug_package %{nil}

Name:           gsettings-desktop-schemas
Version:        45.0
Release:        1
Summary:        A collection of GSettings schemas
License:        LGPLv2+
URL:            https://gitlab.gnome.org/GNOME/gsettings-desktop-schemas
Source0:        https://download.gnome.org/sources/%{name}/45/%{name}-%{version}.tar.xz
Patch0001:	0001-Using-monospace-11-as-monospace-font-name.patch

BuildRequires:  gettext
BuildRequires:  glib2-devel >= 2.31.0
BuildRequires:  gobject-introspection-devel
BuildRequires:  meson

Requires:       glib2 >= 2.31.0

Recommends: font(cantarell)
Recommends: font(sourcecodepro)

%description
gsettings-desktop-schemas contains a collection of GSettings schemas for
settings shared by various components of a desktop.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries
and header files for developing applications that use %{name}.

%package_help

%prep
%autosetup -p1 -n %{name}-%{version}

%build
%meson
%meson_build

%install
%meson_install

%find_lang %{name} --with-gnome

%check
glib-compile-schemas --dry-run --strict %{buildroot}%{_datadir}/glib-2.0/schemas

%files -f %{name}.lang
%doc AUTHORS NEWS
%license COPYING
%{_datadir}/glib-2.0/schemas/*
%{_datadir}/GConf/gsettings/gsettings-desktop-schemas.convert
%{_datadir}/GConf/gsettings/wm-schemas.convert
%{_libdir}/girepository-1.0/GDesktopEnums-3.0.typelib

%files devel
%{_includedir}/*
%{_datadir}/pkgconfig/*
%{_datadir}/gir-1.0/GDesktopEnums-3.0.gir

%files help
%doc HACKING README MAINTAINERS

%changelog
* Mon Jan 8 2024 zhangpan <zhangpan103@h-partners.com> - 45.0-1
- Update to 45.0

* Fri Aug 18 2023 xiaofan <xiaofan@iscas.ac.cn> - 44.0-2
- Change monospace-font-name to 'monospace 11' from 'Source Code Pro 10'

* Wed Jul 19 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 44.0-1
- Update to 44.0

* Mon Jan 02 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 43.0-1
- Update to 43.0

* Wed Apr 20 2022 dillon chen <dillon.chen@gmail.com> - 42.0-1
- Upgrade to 42.0

* Thu Dec 2 2021 hanhui <hanhui15@huawei.com> - 41.0-1
- update to 41.0

* Sat Jan 30 2021 yanglu60 <yanglu60@huawei.com> - 3.38.0-1
- update to 3.38.0

* Wed Jul 22 2020 hanhui <hanhui15@huawei.com> - 3.37.1-1
- update to 3.37.1

* Tue Jan 7 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.34.0-1
- update to 3.34.0

* Tue Aug 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.32.0-1
- Package Init
